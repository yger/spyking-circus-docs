SpyKING CIRCUS
==============

*A fast and scalable solution for spike sorting of large-scale extracellular recordings*

SpyKING CIRCUS is currently still under development. We currently consider this software 
to be in the beta status, please report issues at the github issue tracker

Documentation for SpyKING CIRCUS can be found at http://www.yger.net/software/spyking-circus

The SpyKING CIRCUS is released under the terms of the CeCILL 2.1 license.